import React from 'react'
import { TouchableHighlight } from 'react-native'
import Title from './Title'

export default (props) => {
  return <TouchableHighlight
    onPress={props.onPress}
    underlayColor={'rgba(0,0,0,0.1)'}
    >
    <Title color="blue" {...props} />
  </TouchableHighlight>
}