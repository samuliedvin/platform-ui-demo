import React from 'react'
import { View, Button, Image, Vibration } from 'react-native'
import Title from './Title'
import GestureRecognizer from 'react-native-swipe-gestures'
import Sound from 'react-native-sound'
Sound.setCategory('Playback');

const demoSound = 'https://ia800500.us.archive.org/5/items/aesop_fables_volume_one_librivox/fables_01_00_aesop_64kb.mp3'

export default class Listen extends React.Component {

  static navigationOptions = {
    title: 'Listening',
    headerStyle: {
      backgroundColor: '#111',
      color: 'white',
    },
    headerTitleStyle: {
      fontFamily: 'serif',
      fontWeight: 'bold',
      color: 'white',
    }
  };

  constructor(props) {
    super(props)
    this.userFeedback = this.userFeedback.bind(this)

    this.state = {
      showGestures: false,
    }
  }

  userFeedback(answer) {
    Vibration.vibrate(100)
    if (Math.random() > 0.5) {
      this.correctAudio.play()
    } else {
      this.incorrectAudio.play()
    }
  }

  componentWillMount() {
    let audio = new Sound(demoSound, null, err => {
      if (err) return console.log(err)
      audio.play(success => {
      })
    })
    this.audio = audio

    this.correctAudio = new Sound(require('../assets/yes.mp3'))
    this.incorrectAudio = new Sound(require('../assets/no.mp3'))

    this.userFeedback = this.userFeedback.bind(this)
  }

  componentWillUnmount() {
    this.audio.stop()
    this.audio.release()
  }

  render() {
    let lessonData = this.props.navigation.getParam('lessonData')
    let courseData = this.props.navigation.getParam('courseData')
    return <GestureRecognizer
      style={{
        flex: 1,
        backgroundColor: 'black',
        justifyContent: 'space-between',
      }}
      onSwipeLeft={() => this.userFeedback('no')}
      onSwipeRight={() => this.userFeedback('yes')}
    >
      <Image
        source={{ uri: courseData.image }}
        style={{ width: 40, height: 40 }} />
      {
        this.state.showGestures ?
          <View>
            <Title color='white' fontSize={20}>A or YES - Swipe right</Title>
            <Title color='white' fontSize={20}>B or NO - Swipe left</Title>
          </View> :
          <Title color='white' fontSize={20}>{'Listening to ' + courseData.title + ' - ' + lessonData.title}</Title>
      }
      <Button onPress={() => {
        this.setState((prevState) => {
          return { showGestures: !prevState.showGestures }
        })
      }} title="Gestures" />
    </GestureRecognizer>
  }
}