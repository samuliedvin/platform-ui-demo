import React from 'react'
import { View, Image, Dimensions, Text } from 'react-native'
import Title from './Title'
import LessonLink from './LessonLink'
import ListGroup from './ListGroup';

export default (props) => {
  let navigate = props.navigation.navigate
  let courseData = props.navigation.getParam('courseData')
  return <View>
    <Image
        source={{uri: courseData.image}} style={{
          width: Dimensions.get('window').width,
          height: Dimensions.get('window').width,
          }} />
    <View style={{ margin: 10 }}>
      <Title fontSize={30}>{courseData.title}</Title>
      <Text style={{margin:5}} >{courseData.description}</Text>
      <ListGroup title="Lessons">
          {courseData.lessons.map(lesson => {
            return <LessonLink
              navigate={navigate}
              lessonData={lesson}
              courseData={courseData} />
          })}
      </ListGroup>
    </View>
  </View>
}