import React from 'react'
import { View } from 'react-native'
import NiceButton from './NiceButton'

export default class Lessons extends React.Component {
  static navigationOptions = {
    title: 'Menu',
  };

  render() {
    return (
      <View style={{
        flex: 1,
        justifyContent: 'center',
      }}>
        <NiceButton
          onPress={() => this.props.navigation.navigate('Learn')}>
          Learn
        </NiceButton>
      </View>
    );
  }
}