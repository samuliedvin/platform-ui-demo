import React from 'react'
import { Text } from 'react-native'

export default (props) => {
  return <Text
    style={{
      alignSelf: props.align || 'center',
      color: props.color || 'black',
      fontSize: props.fontSize || 40,
      fontFamily: 'serif',
      textAlign: 'center',
    }}>
    {props.children}
  </Text>
}