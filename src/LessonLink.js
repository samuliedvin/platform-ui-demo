import React from 'react'
import { Text, View, TouchableHighlight, Image } from 'react-native'
import Title from './Title'

export default (props) => {
  return <TouchableHighlight
    onPress={() => {
      props.navigate('Listen', {
        lessonData: props.lessonData,
        courseData: props.courseData,
      })
    }} 
    underlayColor={'rgba(0,0,0,0.1)'}>
    
    <View style={{
      flexDirection: 'row',
      alignItems: 'center',
      backgroundColor: 'rgba(0,0,0,0.02)',
      margin: 5,
      padding: 5,
    }}>
      <Title fontSize={20} align={'flex-start'}>{props.lessonData.title}</Title>
    </View>
  </TouchableHighlight>
}