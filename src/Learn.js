import React from 'react'
import { View } from 'react-native'
import Title from './Title'
import ListGroup from './ListGroup'
import CourseLink from './CourseLink';

let webDev1 = {
  title: 'Essentials of Web Development',
  author: 'University of Turku',
  duration: '15:21',
  durationLeft: '4:00',
  image: 'https://cdn-images-1.medium.com/max/1600/1*HP8l7LMMt7Sh5UoO1T-yLQ.png',
  description: 'Learn the fundamentals of modern Web Development techiniques and the common practicies that surrond the field.',
  lessons: [
    {
      title: 'HTML'
    },
    {
      title: 'CSS'
    },
    {
      title: 'Frameworks'
    },
    {
      title: 'Test driven development'
    },
  ]
}
let philosophy1 = {
  title: 'Philosophy 101',
  author: 'Harvard University',
  duration: '12:41',
  durationLeft: '10:00',
  image: 'https://i.kym-cdn.com/photos/images/original/001/153/172/713.jpeg',
  lessons: [],
}
let cooking1 = {
  title: 'Cooking with Kate',
  author: 'Kate Goodman',
  duration: '5:55',
  durationLeft: '5:55',
  image: 'https://usateatsiptrip.files.wordpress.com/2018/03/gettyimages-887636042.jpg?w=1000&h=600&crop=1',
  lessons: [],
}

export default class Learn extends React.Component {
  render() {
    let navigate = this.props.navigation.navigate
    return (
      <View>
        <Title>Lessons</Title>
        <ListGroup title="Incomplete">
          <CourseLink navigate={navigate} courseData={webDev1}/>
          <CourseLink navigate={navigate} courseData={philosophy1}/>
        </ListGroup>
        <ListGroup title="Featured">
          <CourseLink navigate={navigate} courseData={cooking1}/>
        </ListGroup>
      </View>
    );
  }
}