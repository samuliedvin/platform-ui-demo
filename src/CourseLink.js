import React from 'react'
import { Text, View, TouchableHighlight, Image } from 'react-native'
import Title from './Title'

export default (props) => {
  let data = props.courseData
  return <TouchableHighlight
    onPress={() => {
      props.navigate('Lessons', {
        courseData: data,
      })
    }} 
    underlayColor={'rgba(0,0,0,0.1)'}>
    
    <View style={{
      flexDirection: 'row',
      alignItems: 'center',
      backgroundColor: 'rgba(0,0,0,0.02)',
      margin: 5,
    }}>
      <Image
        source={{uri: data.image}} style={{width: 60, height: 60}} />
      <View style={{
        flex:1,
        padding: 8,
        alignItems: 'flex-start',
      }}>
        <Title fontSize={20} align={'flex-start'}>{data.title}</Title>
        <Text>{"By " + data.author}</Text>
      </View>
    </View>
  </TouchableHighlight>
}