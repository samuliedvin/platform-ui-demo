import { createStackNavigator, createAppContainer } from "react-navigation"
import Menu from 'src/Menu'
import Learn from 'src/Learn'
import Lessons from 'src/Lessons'
import Listen from 'src/Listen'

const AppNavigation = createStackNavigator(
  {
    Menu: {screen: Menu},
    Learn: {screen: Learn},
    Lessons: {screen: Lessons},
    Listen: {screen: Listen},
  },
  {
    initialRouteName : 'Menu',
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#EEE',
      },
      headerTintColor: '#AAA',
      headerTitleStyle: {
        fontFamily: 'serif',
        fontWeight: 'bold',
        color: 'black',
      },
    },
  },
);

export default createAppContainer(AppNavigation)